from time import sleep, perf_counter
from threading import Thread

def func(num):
    print('Thread 1 {num}')
    sleep(1)
    print('Thread 2 {num}')


start_time = perf_counter()

threads = []

for n in range(1, 10):
    t = Thread(target=func, arg=(n,))
    threads.append(t)
    t.start()

for t in threads:
    t.join()

end_time = perf_counter()

print(f'{end_time- start_time: 0.2f}')
